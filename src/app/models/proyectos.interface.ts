export class proyecto{
    constructor(
      public  nombre: string,
      public descripcion: string,
      public publicacion: Date,
      public cierre: Date,
      public criterios:string,
      public recompensa:number,
      public email:string,
      public telefono:string,
      public id_user:number,
      public id_autor:number,
      public activo?: number,
      public  id?: number
    ){}
}